package muPackage.week1.designPattern;

public class Main {
    public static void main(String[] args) {

        MallerdDuck mallerdDuck= new MallerdDuck();
        RubberDuck rubberDuck = new RubberDuck();



        rubberDuck.setFlyBehavior(new FlyWithRocket());
        rubberDuck.performFly();
        mallerdDuck.performFly();
        rubberDuck.performQuack();
    }
}
