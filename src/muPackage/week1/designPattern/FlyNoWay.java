package muPackage.week1.designPattern;

public class FlyNoWay implements Flyable {
    @Override
    public void fly() {
        System.out.println("I am not flying");
    }
}
