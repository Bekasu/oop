package muPackage.week1.designPattern;

public class Quack implements Quackable {
    @Override
    public void quack() {
        System.out.println("I am quacking");
    }
}
