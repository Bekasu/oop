package muPackage.week1.designPattern;

public abstract class Duck {
    Quackable quackBehavior;
    Flyable flyBehavior;

    public Quackable getQuackBehavior() {
        return quackBehavior;
    }

    public void setQuackBehavior(Quackable quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

    public Flyable getFlyBehavior() {
        return flyBehavior;
    }

    public void setFlyBehavior(Flyable flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void performQuack(){
        quackBehavior.quack();
    }

    public void performFly(){
        flyBehavior.fly();
    }
    public void swim(){
        System.out.println("I am swimming");
    }
    public void display(){
        System.out.println("I am a duck");
    }


}
