package muPackage.week1.designPattern;

public class Squick implements Quackable {
    @Override
    public void quack() {
        System.out.println("I am squicking");
    }
}
