package muPackage.week1.designPattern;

public class FlyWithRocket implements Flyable {
    @Override
    public void fly() {
        System.out.println("I am flying with rocket");
    }
}
