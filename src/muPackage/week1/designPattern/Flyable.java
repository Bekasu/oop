package muPackage.week1.designPattern;

public interface Flyable {
     void fly();
}
