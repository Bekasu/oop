package muPackage.week1.designPattern;

public class RubberDuck extends Duck{

    public RubberDuck(){
        quackBehavior = new Squick();
        flyBehavior = new FlyNoWay();
    }
    @Override
    public void display() {
        System.out.println("I am RubberDuck");
    }
}
