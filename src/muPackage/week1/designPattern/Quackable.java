package muPackage.week1.designPattern;

public interface Quackable {
    void quack();
}
