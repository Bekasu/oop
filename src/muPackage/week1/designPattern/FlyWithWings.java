package muPackage.week1.designPattern;

public class FlyWithWings implements Flyable {
    @Override
    public void fly() {
        System.out.println("I am flying with wings");
    }
}
