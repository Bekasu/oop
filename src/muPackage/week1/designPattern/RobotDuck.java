package muPackage.week1.designPattern;

public class RobotDuck extends Duck {
    public RobotDuck(){
        flyBehavior = new FlyWithRocket();

    }
}
