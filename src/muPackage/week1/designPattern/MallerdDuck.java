package muPackage.week1.designPattern;

public class MallerdDuck extends Duck {

    public MallerdDuck(){
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }
    @Override
    public void display() {
        System.out.println("I am Mallard duck");
    }
}
