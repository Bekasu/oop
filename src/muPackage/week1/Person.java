package muPackage.week1;

public class Person {
    private String name;
    private int birthYear;
    private String info;

    public Person(){}

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int age(){
        return 2020-birthYear;
    }

    public void input(String str){
        this.info = str;
    }

    public String output(){
        return info;
    }


}
