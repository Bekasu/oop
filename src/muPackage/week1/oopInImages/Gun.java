package muPackage.week1.oopInImages;

public class Gun {
    int size;
    int bulletsInMagazine;

    public Gun(int size){
        this.size = size;
        reload();
    }

    public void fire(){
        bulletsInMagazine--;
    }

    public void reload(){
        bulletsInMagazine = size;
    }
}
