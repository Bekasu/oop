package muPackage.week1.oopInImages;

public class Main {
    public static void main(String[] args) {
        Transformer optimus = new Transformer(0);
        optimus.fire();
        optimus.run();
        System.out.println(optimus.position);
    }
}
