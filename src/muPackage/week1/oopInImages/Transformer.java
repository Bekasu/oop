package muPackage.week1.oopInImages;

public class Transformer {
    int position;
    Gun left;
    Gun right;

    public Transformer(int position){
        this.position = position;
        right = new Gun(18);
        left = new Gun(30);
    }

    public  void fire(){
        left.fire();
        right.fire();
    }
    public void run(){
        this.position = this.position + 1;
    }
}
