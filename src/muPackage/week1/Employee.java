package muPackage.week1;

public class Employee {
    private String name;
    private int rate;
    private double hour;
    static double totalSum = 0;

    public Employee() {}

    public Employee(String name, int rate) {
        this.name = name;
        this.rate = rate;
    }

    public Employee(String name, int rate, double hour) {
        this.name = name;
        this.rate = rate;
        this.hour = hour;
        totalSum += hour;
    }
    public double getTotalSum(){
        return totalSum;
    }

    public double salary(){
        return rate*hour;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                ", hour=" + hour +
                '}';
    }
    public void changeRate(int rate){
        this.rate = rate;
    }

    public double bonuses(){
        return salary()*0.1;
    }
}
