package muPackage.week2.Character_homet;

public class KnifeBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("I've got knife");
    }
}
