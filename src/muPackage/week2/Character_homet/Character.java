package muPackage.week2.Character_homet;

public abstract class Character {
    WeaponBehavior weapon;

    public void fight(){
        weapon.useWeapon();
    }

    public void setWeapon(WeaponBehavior w){
        this.weapon = w;
    }

}
