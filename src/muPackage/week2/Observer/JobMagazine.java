package muPackage.week2.Observer;

import java.util.ArrayList;
import java.util.List;

public class JobMagazine implements Subject {
    private List<Observer> subscriber = new ArrayList<>();
    private List<String> vacancies = new ArrayList<>();

    public JobMagazine() {
        subscriber = new ArrayList<>();
        vacancies = new ArrayList<>();
    }

    public void addVacancy(String vacancy){
        vacancies.add(vacancy);
        notifyAllObservers();
    }

    public void removeVacancy(String vacancy){
        vacancies.remove(vacancy);
        notifyAllObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        subscriber.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        subscriber.remove(observer);
    }

    @Override
    public void notifyAllObservers() {
        for (Observer observer : subscriber){
            observer.handleEvent(vacancies);
        }
    }
}
