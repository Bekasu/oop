package muPackage.week2.Observer;

public class Main {
    public static void main(String[] args) {
        JobMagazine HeadHunter = new JobMagazine();
        HeadHunter.addVacancy("Junior Java Developer");
        HeadHunter.addVacancy("Senior PHP Developer");

        Subscriber alisher = new Subscriber("Alisher");
        Subscriber venera = new Subscriber("Venera");

        HeadHunter.addObserver(alisher);
        HeadHunter.addObserver(venera);

        HeadHunter.addVacancy("Go lang developer");
        HeadHunter.removeObserver(alisher);
        HeadHunter.removeVacancy("Junior Java Developer");
    }
}
