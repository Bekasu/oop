package muPackage.week2.FileReader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Reader {
    public String readFromFile(String myText) {
        try (
                FileInputStream myFile = new FileInputStream("src\\muPackage\\practices\\week2\\FileReader\\lab.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(myFile, "UTF-8");
                BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String nextLine;

            char lf = 0x0A;
            String endLine = "" + lf;

            boolean eof = false;
            while (!eof) {
                nextLine = reader.readLine();
                if (nextLine == null) {
                    eof = true;
                } else {
                    myText += nextLine + endLine;
                    // myTextArray.add(nextLine);
                }
            }
            System.out.println(myText);
        } catch (
                IOException e) {
            System.out.println("Can't read Stalking.txt");
        }
        return myText;
    }
}
