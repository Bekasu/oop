package muPackage.week2.FileReader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class Loader {
    public void loadToFile(String myText){
        try {
            System.out.println(myText);
            FileOutputStream myFile = new FileOutputStream("copy.txt");
            PrintWriter out = new PrintWriter(myFile);
            out.print(myText);
            out.close();
        } catch (IOException e){
            System.out.println("Don't write date to File");
        }
    }
}
