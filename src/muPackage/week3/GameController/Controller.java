package muPackage.week3.GameController;

public interface Controller {

    void connect();
    void keyPressed();

}
