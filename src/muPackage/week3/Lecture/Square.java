package muPackage.week3.Lecture;

public class Square extends Shape {
    int side = 5;



    @Override
    void area() {
        System.out.println("Area of square = " + side*side);
    }

    @Override
    void display() {
        System.out.println("Draw a Square");
    }
}
