package muPackage.week3.Lecture;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
       Shape shape1 = new Circle();
       Shape shape2 = new Square();

       List<Shape> list = new ArrayList<>();
       list.add(shape1);
       list.add(shape2);

       for (Shape shape : list){
           shape.display();
       }
    }
}
