package muPackage.week3.WeatherStation;

public class Current implements Display,Observer {

    private float temperature;
    private float humidity;
    private float pressure;
    private Subject weatherData;

    public Current(Subject weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void Display() {
        System.out.println("Temperatute - " + temperature);
        System.out.println("Humidity - " + humidity);
        System.out.println("Pressure - " + pressure);
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
    }
}
