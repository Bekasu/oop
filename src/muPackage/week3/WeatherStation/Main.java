package muPackage.week3.WeatherStation;

public class Main {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        Current current = new Current(weatherData);
        weatherData.setMeasurements(10,10,10);
        current.Display();
        weatherData.setHumidity(1500);
        current.Display();
    }
}
