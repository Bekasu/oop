package muPackage.week3.WeatherStation;

import java.util.ArrayList;
import java.util.List;

public class WeatherData implements Subject {
    private float temperature;
    private float humidity;
    private float pressure;
    private ArrayList ListObservers;

    public WeatherData(){
        this.ListObservers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        ListObservers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        if (ListObservers.indexOf(observer) >= 0){
            ListObservers.remove(observer);
        }
    }

    @Override
    public void notifyObserver() {
        for (int i = 0; i < ListObservers.size(); i++)
            {
                Observer observer = (Observer)ListObservers.get(i);
                observer.update(temperature,humidity,pressure);
        }
    }

    public void measurementsChanged(){
        notifyObserver();
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
        measurementsChanged();
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
        measurementsChanged();
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
        measurementsChanged();
    }

    public void setMeasurements(float temperature, float humidity, float pressure){
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        measurementsChanged();
    }
}
