package muPackage.Week4;

public class ErrorWithDocument extends Exception{
    public ErrorWithDocument(String message){
        super(message);
    }
}
