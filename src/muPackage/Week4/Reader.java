package muPackage.Week4;

public class Reader {
    private int CountOfPages;
    private boolean isCoffeeReady;
    private boolean isLightsOn;
    private boolean isMusicTurnedOn;

    public Reader(int CountOfPages){
        this.CountOfPages = CountOfPages;
    }

    public void setCoffeeReady() {
        isCoffeeReady = true;
        System.out.println("Ur tasty coffee is ready");
    }

    public void setLightsOn() {
        isLightsOn = true;
        System.out.println("Light turned on");
    }

    public void setMusicTurnedOn() {
        isMusicTurnedOn = true;
        System.out.println("Hormonic music set up");
    }

    public void read() throws ErrorWithDocument{
        if(isCoffeeReady && isLightsOn && isMusicTurnedOn){
            System.out.println("U can read ur book peacefully");
        } else {
            throw new ErrorWithDocument("Your conditions for reading a book aren't meet");
        }
    }
}
