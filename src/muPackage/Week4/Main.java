package muPackage.Week4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) throws IOException{
        Reader Bek = new Reader(15);
        Bek.setCoffeeReady();
        Bek.setLightsOn();
        Bek.setMusicTurnedOn();

        try {
            Bek.read();
        } catch (ErrorWithDocument e){
            File file = new File("log.txt");
            FileWriter w = new FileWriter(file,true);
            PrintWriter p = new PrintWriter(w);
            p.println(e.getMessage());
            p.close();
        }
    }
}
