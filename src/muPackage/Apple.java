package muPackage;

public class Apple {
    //Field or STATE
    String color;
    double weight;
    boolean isTasty;

    //constructor
    public Apple(String color){
        this.color = color;
    }
    public Apple(){}

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isTasty() {
        return isTasty;
    }

    public void setTasty(boolean tasty) {
        isTasty = tasty;
    }

    //method(1# - public, 2# - private)
    void collectApple(){}


}
